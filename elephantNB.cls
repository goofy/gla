% Bienvenue sur la classe elephant.
\ProvidesClass{elephant}[2012/03/31]

% elephant est une classe LaTeX moderne.
\NeedsTeXFormat{LaTeX2e}

% Elle se base pour l'essentiel sur la classe LaTeX book.
\LoadClass[10pt,twoside,twocolumn,french]{book}

%------------------ Au cas ou les effets de calques ne fonctionnent pas
% avec l'imprimeur, il faut downgrader la version PDF. Par contre cela suppose
% de revoir toutes les illustrations
%\pdfminorversion=3

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dépendances générales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Insérer des illustrations en plein dans les colonnes
\RequirePackage{wrapfig}
% Aide sur wrapfig en deux colonnes: http://tex.stackexchange.com/questions/45709/how-do-you-create-pull-quotes
\RequirePackage{calc}


%%%%%%%%%%%%%%%%%%%
%% Marquer un début de paragraphe quand on veut sur col gauche
% avec wrapfig

\newcommand{\debutpar}{
\begin{wrapfigure}[3]{l}{0.5cm}
\fontsize{40}{40}\selectfont{\textcolor{noir}{\ding{122}}}
\end{wrapfigure} \noindent
}


% Nous écrivons en unicode.
\RequirePackage[utf8]{inputenc}

% Nous utilisons de jolies fontes vectorielles.

\renewcommand{\familydefault}{\sfdefault}
\renewcommand{\sfdefault}{phv}
\RequirePackage[T1]{fontenc}


% Nous étendons LaTeX avec le symbole €.
\RequirePackage{eurosym}
\DeclareUnicodeCharacter{20AC}{{\euro}}

% Nous aurons besoin de quadrichromie,
\RequirePackage[cmyk,usenames,dvipsnames]{xcolor}
% ainsi que de différentes subtilités graphiques.
\RequirePackage{pgf}
\RequirePackage{tikz}
\usetikzlibrary{shapes,snakes,shadows,arrows}
\RequirePackage{amssymb}

% Le paquet environ permet facilement de switcher entre macro et environnement.
\usepackage{environ}

% Le paquet ifthen facilite le contrôle de flux.
\usepackage{ifthen}
\usepackage[strict]{changepage}

% Nous définissons de nouveaux types de flottants en nous appuyant sur le paquet
% float.
\RequirePackage{float}

% Nous éditons les hyperliens, avec césure, unicode, et sans cadre.
\RequirePackage[hyphens]{url}
\RequirePackage[breaklinks=true, unicode=True, pdfborder={0 0 0}]{hyperref}
\def\UrlNoBreaks{\do\(\do\[\do\{\do\<\do\:}%

% Le paquet microtype permet les fonctionnalités de microtypographie.
% \RequirePackage{microtype}

% Et finalement, nous mettrons en oeuvre les subtilités de la typographie
% française.
\RequirePackage[french]{babel}

% pour changer à volonté les puces des listes à puces
\RequirePackage{enumitem}

% Et tant qu'on y est, le package pifont nous permet d'enter des caractères rigolos
\RequirePackage{pifont}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chemins
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\@includes{includes/}
\def\@images{\@includes images/}
\def\@screenshots{\@includes screenshots/}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mise en page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Couleurs 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%








%%% Les bonnes couleurs

% Solution : "pause café"
%\definecolor{couleur1}{RGB}{170,68,0} %  orange
%\definecolor{couleur2}{RGB}{191,214,105} %  vert
%\definecolor{couleur3}{RGB}{85,68,0} %  chocolat
%\definecolor{couleur4}{RGB}{238,244,214} %  vert tres clair pour le fond
%\definecolor{gris}{cmyk}{0,0,0,0.45}
%\definecolor{grisfonce}{cmyk}{0,0,0,0.70}

% Solution Framacolors
\definecolor{couleur1}{cmyk}{0,0.542,0.797,0.247} % Frama orange
\definecolor{couleur2}{cmyk}{0,0.296,0.862,0.231} % frama jaune
\definecolor{couleur3}{cmyk}{0,0.754,0.918,0.329} % framarouge
\definecolor{couleur4}{cmyk}{0,0,0,0} % du blanc
\definecolor{couleur5}{cmyk}{0.892,0.19,0,0.38} % framableu
\definecolor{couleur6}{cmyk}{0.0897,0,0.538,0.388} % framavert
\definecolor{grisfonce}{cmyk}{0,0,0,0.62} %framagris fonce
\definecolor{gris}{cmyk}{0,0,0,0.20} % framagris clair
\definecolor{noir}{cmyk}{0,0,0,1} % noir c'est noir

% Colorer les fonds de pages
%\RequirePackage{xcolor}
%\pagecolor{couleur4}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




\RequirePackage[
paperheight=216.3mm,
paperwidth=151.3mm,
margin=6.3mm,
textheight=197.1mm,
textwidth=135.4mm,
top=20mm,
bottom=20mm,
inner=20mm,
outer=15mm,
headsep=8mm,
twoside,
%showframe=true,
%showcrop=true
]{geometry}




% pour faire ce qu'on veut avec les caption
\RequirePackage{caption}
\captionsetup[figure]{labelfont={color=noir,small,sf},textfont={color=noir,small,sf,it},justification=centering}


% Headers et footers discrets
\pagestyle{plain}%

% Pas de césure pour les mots en majuscule.
\uchyph=0




%%% Autres interfaces %%%

% Pour l'écrit étranger.
\newcommand{\foreign}[1]{{\it #1}}
\let\etranger\foreign

% Pour les marques.
\newcommand{\brand}[1]{{#1}}
\let\marque\brand

% Pour les aspects techniques.
\newcommand{\tech}[1]{{\tt #1}}
\let\info\tech
\let\shell\tech
\let\command\tech

% Les e-mail

\newcommand{\email}[1]{\href{mailto:#1}{\tech{#1}}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mise en forme
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% espacer les paragraphes
\setlength{\parskip}{2mm}


% emphase - italique????
\renewcommand{\emph}[1]{{\it#1}}


%% Mettre en couleur les \strong
\newcommand{\strong}[1]{\ding{229}~{#1}
}

%%%% poser une question dans les interviews
\newcommand{\question}[1]{
\textbf{\textcolor{noir}{#1} \\ \nobreak}
}



%%% Hyperliens %%%


% La coloration des liens est sans intérêt en général (on le fera à plus haut
% niveau si nécessaire).
\hypersetup{colorlinks=false}

\newcommand\fnurl[2]{%
\href{#2}{#1}\footnote{\url{#2}.}%
}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% En-tetes des chapitres
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%----------------------------------------------------------------------------------------
%	CHAPTER HEADINGS
%----------------------------------------------------------------------------------------







%%%%%%%%%%%%%% Background pages
\RequirePackage{ifoddpage}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\lesloganduchapitre}{}%
\newcommand{\sloganduchapitre}[1]{\renewcommand{\lesloganduchapitre}{#1}}%

\newcommand{\leptitbonhomme}{}%
\newcommand{\ptitbonhomme}[1]{\renewcommand{\leptitbonhomme}{#1}}%

\def\@makechapterhead#1{%
{\parindent \z@ \raggedright \normalfont
\ifnum \c@secnumdepth >\m@ne
\if@mainmatter
\begin{tikzpicture}[remember picture,overlay]
\node at (current page.center)
{
\tikzset{grid/.style={black,very thin,opacity=0}} %regler opacity à 0.5 pour afficher grille
\begin{tikzpicture}[scale=0.5] % mettre à 0.4 pour dessiner et monter à 0.5 pour terminer
\draw[grid] (0,0) grid (31,40); %decommenter pour afficher grille
\draw [fill=grisfonce, draw=grisfonce]  (0,38) -- (31,38) -- (31,31) -- (0,31);
\draw [fill=noir, draw=noir,] (0,38) -- (11,38) -- (11,36) -- (11.5,35.5) -- (11,35) -- (11,31) -- (0,31);
\draw [fill=none, draw=white, line width=2pt] (11,38) -- (11,36) -- (11.5,35.5) -- (11,35) -- (11,31);
\node [text=couleur4] (numchap) at (7,34.5) {\fontsize{144}{144}\selectfont{\thechapter}};
\node (titrechap) [rectangle,text=couleur4,align=left] at (18,35.5) {
\begin{minipage}{4cm}
\raggedright\fontsize{15}{15}\selectfont\bf{#1}
\end{minipage}
};
\node (guillemets) [text=black] at (12.5,30) {
\includegraphics[scale=0.1]{\leptitbonhomme}
};
\node (sloganchap) [text=noir, align=left] at (20,29) {
\begin{minipage}{4cm}
\raggedright\tt\fontsize{8}{8}\selectfont{\lesloganduchapitre}
\end{minipage}
};
\end{tikzpicture}};
\end{tikzpicture}
\else
\begin{tikzpicture}[remember picture,overlay]
\node at (current page.center)
{
\tikzset{grid/.style={black,very thin,opacity=0}} %regler opacity à 0.5 pour afficher grille
\begin{tikzpicture}[scale=0.5] % mettre à 0.4 pour dessiner et monter à 0.5 pour terminer
\draw[grid] (0,0) grid (31,40); %decommenter pour afficher grille
\draw [fill=grisfonce, draw=grisfonce]  (0,38) -- (31,38) -- (31,31) -- (0,31);
\draw [fill=noir, draw=noir,] (0,38) -- (11,38) -- (11,36) -- (11.5,35.5) -- (11,35) -- (11,31) -- (0,31);
\draw [fill=none, draw=white, line width=2pt] (11,38) -- (11,36) -- (11.5,35.5) -- (11,35) -- (11,31);
\node [text=couleur4] (numchap) at (7,34.5) {\fontsize{144}{144}\selectfont{\#}};
\node (titrechap) [rectangle,text=couleur4,align=left] at (18,35.5) {
\begin{minipage}{4cm}
\raggedright\fontsize{17}{17}\selectfont\bf{#1}
\end{minipage}
};
\node (guillemets) [text=black] at (12.5,30) {
\includegraphics[scale=0.1]{\leptitbonhomme}
};
\node (sloganchap) [text=noir] at (20,29) {
\begin{minipage}{4cm}
\raggedright\tt\fontsize{8}{8}\selectfont{\lesloganduchapitre}
\end{minipage}
};
\end{tikzpicture}};
\end{tikzpicture}
\fi\fi\par\vspace*{190\p@}}}

%-------------------------------------------

\def\@makeschapterhead#1{%
\begin{tikzpicture}[remember picture,overlay]
\node at (current page.center)
{
\tikzset{grid/.style={black,very thin,opacity=0}} %regler opacity à 0.5 pour afficher grille
\begin{tikzpicture}[scale=0.5] % mettre à 0.4 pour dessiner et monter à 0.5 pour terminer
\draw[grid] (0,0) grid (31,40); %decommenter pour afficher grille
\draw [fill=grisfonce, draw=grisfonce]  (0,38) -- (31,38) -- (31,31) -- (0,31);
\draw [fill=noir, draw=noir,] (0,38) -- (11,38) -- (11,36) -- (11.5,35.5) -- (11,35) -- (11,31) -- (0,31);
\draw [fill=none, draw=white, line width=2pt] (11,38) -- (11,36) -- (11.5,35.5) -- (11,35) -- (11,31);
\node [text=couleur4] (numchap) at (7,34.5) {\fontsize{144}{144}\selectfont{\#}};
\node (titrechap) [rectangle,text=couleur4,align=left] at (18,35.5) {
\begin{minipage}{4cm}
\raggedright\fontsize{15}{15}\selectfont\bf{#1}
\end{minipage}
};
\node (guillemets) [text=black] at (12.5,30) {
\includegraphics[scale=0.3]{\leptitbonhomme}
};
\node (sloganchap) [text=noir] at (20,29) {
\begin{minipage}{4cm}
\raggedright\tt\fontsize{8}{8}\selectfont{\lesloganduchapitre}
\end{minipage}
};
\end{tikzpicture}};
\end{tikzpicture}
\par\vspace*{190\p@}}
\makeatother

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Les sections, sous-sections, titres de paragraphes

\RequirePackage{titlesec}

\titleformat{\section}[display]
  {\normalfont\huge\bfseries}{}{2pt}{\filcenter\Large\color{noir}}

%Les espaces verticales avant et après les titres de section
  \titlespacing{\section}{0mm}{0mm}{0mm}

\titleformat{\subsection}[display]
  {\filcenter\normalsize\bfseries}{}{2pt}{\filcenter\normalsize\color{noir}}
  
  %Les espaces verticales avant et après les titres de sous-section
  \titlespacing{\subsection}{0mm}{0mm}{0mm}
  
  
%\titleformat{\paragraph}[runin]
%  {\normalfont\normalsize\bfseries}{}{0pt}{\normalsize\scshape\color{grisfonce}}[~---~]
  
  \titleformat{\paragraph}[runin]
  {\normalfont\normalsize\bfseries}{}{0pt}{\normalsize\color{noir} \ding{110} }[~---~]




%%%% Personaliser la table des matières

\RequirePackage{titletoc}

\titlecontents{chapter}%
[1.5em]% retrait à gauche
{\addvspace{1em plus 0pt}\bfseries\scshape\color{noir}}% matériel avant commun aux entrées numérotées ou pas
{\contentslabel{1.3em}}% avant lorsqu'il y a un numéro
{\hspace{-1.3em}}% avant lorsqu'il n'y a pas de numéro
{\hfill\contentspage}% points de suspension et numéro de page
[\addvspace{0pt}]% matériel après 

% Pour les sections et sous-sections:
\titlecontents{section}%
[3.8em]% retrait à gauche
{\addvspace{0pt}\small}% matériel avant commun aux entrées numérotées ou pas
{\contentslabel{2.3em}}% avant lorsqu'il y a un numéro
{\hspace{-2.3em}}% avant lorsqu'il n'y a pas de numéro
{\titlerule*[0.75em]{.}\contentspage}% points de suspension et numéro de page
[\addvspace{-4pt}]% matériel après


\titlecontents{subsection}%
[4.8em]% retrait à gauche
{\addvspace{-2pt}\footnotesize}% matériel avant commun aux entrées numérotées ou pas
{\contentslabel{3em}}% avant lorsqu'il y a un numéro
{\hspace{-2.3em}}% avant lorsqu'il n'y a pas de numéro
{\titlerule*[0.75em]{.}\contentspage}% points de suspension et numéro de page
[\addvspace{-4pt}]% matériel après



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flottants : logiciels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% En fait le plus simple est de crére des des boites par logiciel déjà pré-construites
%% Je tape par exemple \libreoffice et hop j'ai une boite qui me parle de Libreoffice



% Déclarons les icones utilisées de façon répétitives, pour optimiser l'espace.
\pgfdeclareimage[height=8pt]{hirondelle1}{images/hironmargue/hirondelle1NB}
\pgfdeclareimage[height=8pt]{hirondelle2}{images/hironmargue/hirondelle2NB}
\pgfdeclareimage[height=8pt]{marguerite1}{images/hironmargue/marguerite1NB}
\pgfdeclareimage[height=8pt]{marguerite2}{images/hironmargue/marguerite2NB}


% Insert autant d'image1 que d'étoiles passées en argument. Complète avec autant
% d'image2 que nécessaire. Ce n'est pas codé très intelligemment, mais c'est
% efficace.
\def\iconify#1#2{
  \ifthenelse{\equal{#2}{*}}{
    \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#11}}}
    \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#12}}}
    \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#12}}}
  }
  {
    \ifthenelse{\equal{#2}{**}}{
      \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#11}}}
      \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#11}}}
      \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#12}}}
    }
    {
      \ifthenelse{\equal{#2}{***}}{
        \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#11}}}
        \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#11}}}
        \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#11}}}
      }
      {
        \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#12}}}
        \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#12}}}
        \raisebox{-2pt}{\makebox[1em]{\pgfuseimage{#12}}}
      }
    }
  }
}


%% Reprendre les définition de variable présentes dans la classe initiale pour en faire de simples commandes avec des arguments

%% Commandes pour les autres infos 
\newcommand*{\nom}[1]{\begin{center}\color{noir} \textsc{\textbf{#1}} \\ }
\newcommand*{\logo}[1]{\color{noir} \textbf{Logo :} #1}
\newcommand*{\genre}[1]{\color{noir} #1 \end{center}} % Il s'agissait de la variable description mais la commande \description existe déjà. Il faut éviter un renewcommand...

%%%%% Commandes pour icones margueritte et hirondelle
\newcommand*{\apprentissage}[1]{\begin{itemize}[font=\color{noir}, label=\ding{251}] \item \color{noir} \textit{Apprentissage :} \iconify{hirondelle}{#1}}
\newcommand*{\installation}[1]{\item \color{noir} \textit{Installation :} \iconify{marguerite}{#1} \end{itemize}}



\NewEnviron{encartsimple}{%
\begin{center}
\begin{tikzpicture}
    \node(s){
\begin{minipage}{0.85\columnwidth}
\vspace{2mm}
\sffamily\small
\BODY
\vspace{2mm}
\end{minipage}
};
    \draw[line width=1pt] (s.north west)--(s.north east) (s.south west)--(s.south east);
\end{tikzpicture}
\end{center}
}


%%% Encart simple OLD

%\NewEnviron{encartsimple}{%
%\begin{center}
%\begin{tikzpicture}
%% la boite
%\node[rectangle,rounded corners=5pt,draw=couleur5,fill=none,inner sep=5pt, inner ysep=5pt,line width=1pt] (encartsimple)
%{
%\begin{minipage}{0.8\columnwidth}
%\small
%\BODY
%\end{minipage}
%};
%\end{tikzpicture}
%\end{center}
%}


%%%%%% Encart logiciels %%%%%

\NewEnviron{logiciel}{%
\begin{center}
\begin{tikzpicture}
    \node(s){
\begin{minipage}{0.85\columnwidth}
\vspace{2mm}
\sffamily\small
\BODY
\end{minipage}
};
    \draw[line width=1pt] (s.north west)--(s.north east) (s.south west)--(s.south east);
\end{tikzpicture}
\end{center}
}

%%%%%% Encart logiciels old version%%%%%
%\NewEnviron{logiciel}{%
%\begin{center}
%\begin{tikzpicture}
%% la boite
%\node[rectangle,rounded corners=5pt,draw=noir,fill=none,inner sep=5pt, inner ysep=5pt,line width=1pt] (boitelogiciel)
%{
%\begin{minipage}{0.85\columnwidth}
%\sffamily\small
%\BODY
%\end{minipage}
%};
%\end{tikzpicture}
%\end{center}
%}


%%%%%%%% Encart ressources

%%% pour illustration des ressources

\NewEnviron{ressource}{
\begin{figure}
\centering
\begin{tikzpicture}
% la boite
\node[rectangle,rounded corners=0pt,draw=noir,fill=gris,inner sep=5pt, inner ysep=20pt] (mabox)
{
\begin{minipage}{0.8\columnwidth}
\footnotesize{\BODY}
\end{minipage}
};
% le titre de la boite
\node[fill=noir, rounded corners=0pt, text=white, draw=noir, line width=1pt, rectangle,inner sep=5pt] at (mabox.north) {Pour aller plus loin};
\end{tikzpicture}
\end{figure}
}

%%% les screenshots
\newcommand{\screenshots}[2]{\begin{figure}[H]\centering\includegraphics[width=0.9\columnwidth]{#1}\caption*{#2}\end{figure}}




%%%%%%%%%%%%%%%%%%%%%
% Des boites
%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%% Boitemagique mono-colonne

\definecolor{fondtitre}{RGB}{243,102,25}

\definecolor{fonddeboite}{RGB}{243,102,25}


\newcommand*{\boitemagique}[2]{
\begin{figure}
\centering
\begin{tikzpicture}
\node[rectangle,rounded corners=0pt,draw=noir,fill=gris,inner sep=5pt, inner ysep=15pt,text=black,line width=1pt,minimum width=0.8\columnwidth] (mabox)
{
\begin{minipage}{0.8\columnwidth}
\footnotesize{#2}
\end{minipage}
};

\node[right=-4pt,fill=noir,draw=noir,rounded corners=0pt, text=white,line width=1pt,rectangle,inner sep=5pt] at (mabox.north west) {#1};
\end{tikzpicture}
\end{figure}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% En-tetes, pieds de page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\RequirePackage{emptypage} % supprime les en-tete et pied de page des pages vides

\RequirePackage{fancyhdr}
%% un peu d'aide: http://www.xm1math.net/doculatex/entetepied.html

%% redefinir les entete et pied de page des premieres pages de chapitre
\fancypagestyle{plain}{%
\fancyhf{} % nettoyer tous les champs 
\fancyfoot[C]{} % metre "rien" au centre du pied de page
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}


\pagestyle{fancy}

%% Faire disparaître la ligne sous l'en-tete
\renewcommand{\headrulewidth}{0pt}



\newcommand{\changefont}{%
    \color{couleur4}\bf\fontsize{9}{80}\selectfont
}

\renewcommand{\sectionmark}[1]{\markright{#1}{}}

\newcommand{\piedpagefont}{%
\sffamily
}



% les entêtes du document par défaut

% redefinir les noms de chapitre en enlevant "chapitre 1", "chapitre 2", etc.
\renewcommand{\chaptermark}[1]{\markboth{%
    #1}{}}



%%% On utilise fancypagestyle pour définir des en tete et pieds de page
%%% differents selon frontmatter, mainmatter, et eventuellement backmatter
%%% Pour cela dans le document, sous les commandes \frontmatter etc
%%% il faudra définir le style, par exemple:
%%% \mainmatter
%%% \pagestyle{mainmatter}


% Au milieu, avec une grande barre
\fancyhead[LO,RO,LE,RE]{} % on va utiliser RE pour faire le fond de toute l'entete


\fancypagestyle{frontmatter}{
\fancyhead[CE]{
\begin{tikzpicture}[remember picture,overlay]
\draw [draw=noir,line width=8mm] (-15,-0.3) -- (15,-0.3);
\node[text=couleur4] at (0,-0.3) {\changefont Guide Libre Association};
\end{tikzpicture}
}

\fancyhead[CO]{
\begin{tikzpicture}[remember picture,overlay]
\draw [draw=noir,line width=8mm] (-15,-0.3) -- (15,-0.3);
\node[text=couleur4] at (0,-0.3) {\changefont \leftmark};
\end{tikzpicture}
}

\fancyfoot[C]{
\begin{tikzpicture}
\node[fill=noir, text=couleur4, rectangle] at (0,0) {\changefont \thepage};
\end{tikzpicture}
}

}



\fancypagestyle{mainmatter}{
\fancyhead[CE]{
\begin{tikzpicture}[remember picture,overlay]
\draw [draw=noir,line width=8mm] (-15,-0.3) -- (15,-0.3);
\node[text=couleur4] at (0,-0.3) {\changefont Guide Libre Association};
\end{tikzpicture}
}

\fancyhead[CO]{
\begin{tikzpicture}[remember picture,overlay]
\draw [draw=noir,line width=8mm] (-15,-0.3) -- (15,-0.3);
\node[text=couleur4] at (0,-0.3) {\changefont \leftmark};
\end{tikzpicture}
}

\fancyfoot[C]{
\begin{tikzpicture}
\node[fill=noir, text=couleur4, rectangle] at (0,0) {\changefont \thepage};
\end{tikzpicture}
}
}



\fancypagestyle{backmatter}{
\fancyhead[CE]{
\begin{tikzpicture}[remember picture,overlay]
\draw [draw=noir,line width=8mm] (-15,-0.3) -- (15,-0.3);
\node[text=couleur4] at (0,-0.3) {\changefont Guide Libre Association};
\end{tikzpicture}
}

\fancyhead[CO]{
\begin{tikzpicture}[remember picture,overlay]
\draw [draw=noir,line width=8mm] (-15,-0.3) -- (15,-0.3);
\node[text=couleur4] at (0,-0.3) {\changefont \leftmark};
\end{tikzpicture}
}

\fancyfoot[C]{
\begin{tikzpicture}
\node[fill=noir, text=couleur4, rectangle] at (0,0) {\changefont \thepage};
\end{tikzpicture}
}

}











